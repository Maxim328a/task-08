package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Clas;
import com.epam.rd.java.basic.task8.entities.Student;
import com.epam.rd.java.basic.task8.entities.Subject;
import com.epam.rd.java.basic.task8.entities.Teacher;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Controller for StAX parser.
 */
public class STAXController {
	private static final String CLAS_TAG = "class";
	private static final String NAME_TAG = "name";
	private static final String AGE_TAG = "age";
	private static final String LICENSE_TAG = "license";
	private static final String STUDENT_TAG = "student";
	private static final String SUBJECT_TAG = "subject";
	private static final String TEACHER_TAG = "teacher";


	private Clas clas;

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Clas getResult() {
		doAction();
		return clas;
	}

	private void doAction() {
		clas = new Clas();
		Teacher teacher = new Teacher();
		Student student = new Student();
		Subject subject = new Subject();
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader xmlEventReader =
					xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (xmlEventReader.hasNext()) {
				XMLEvent xmlEvent = xmlEventReader.nextEvent();
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					String currentTag = startElement.getName().getLocalPart();
					switch (currentTag) {
						case CLAS_TAG: {
							Attribute attribute = startElement.getAttributeByName(new QName("name"));
							clas.setName(attribute.getValue());
							break;
						}
						case NAME_TAG:
							xmlEvent = xmlEventReader.nextEvent();
							teacher.setName(xmlEvent.asCharacters().getData());
							break;
						case AGE_TAG:
							xmlEvent = xmlEventReader.nextEvent();
							teacher.setAge(Integer.parseInt(xmlEvent.asCharacters().getData()));
							break;
						case LICENSE_TAG:
							xmlEvent = xmlEventReader.nextEvent();
							teacher.setLicense(Integer.parseInt(xmlEvent.asCharacters().getData()));
							break;
						case STUDENT_TAG: {
							Attribute attribute = startElement.getAttributeByName(new QName("name"));
							student.setName(attribute.getValue());
							attribute = startElement.getAttributeByName(new QName("age"));
							student.setAge(Integer.parseInt(attribute.getValue()));
							break;
						}
						case SUBJECT_TAG: {
							Attribute attribute = startElement.getAttributeByName(new QName("title"));
							subject.setTitle(attribute.getValue());
							attribute = startElement.getAttributeByName(new QName("mark"));
							subject.setMark(Integer.parseInt(attribute.getValue()));
							student.addSubject(subject);
							subject = new Subject();
							break;
						}
						default: break;
					}
				}

				if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					String currentTag = endElement.getName().getLocalPart();
					switch (currentTag) {
						case TEACHER_TAG:
							clas.setTeacher(teacher);
							teacher = new Teacher();
							break;
						case STUDENT_TAG:
							clas.addStudent(student);
							student = new Student();
							break;
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}