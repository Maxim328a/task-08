package com.epam.rd.java.basic.task8.entities;

public class Teacher extends Person{
    private int license;

    public Teacher(String name, int age, int license) {
        super(name, age);
        this.license = license;
    }

    public Teacher() {
        super();
    }

    public int getLicense() {
        return license;
    }

    public void setLicense(int license) {
        this.license = license;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", license=" + license +
                '}';
    }
}
