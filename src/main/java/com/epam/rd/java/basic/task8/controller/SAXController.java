package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Clas;
import com.epam.rd.java.basic.task8.entities.Student;
import com.epam.rd.java.basic.task8.entities.Subject;
import com.epam.rd.java.basic.task8.entities.Teacher;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	private static final String CLAS_TAG = "tn:class";
	private static final String TEACHER_TAG = "teacher";
	private static final String NAME_TAG = "name";
	private static final String AGE_TAG = "age";
	private static final String LICENSE_TAG = "license";
	private static final String STUDENTS_TAG = "students";
	private static final String STUDENT_TAG = "student";
	private static final String SUBJECT_TAG = "subject";

	private String xmlFileName;
	private StringBuilder elementValue;

	private Clas clas;
	private Teacher teacher;
	private Student student;
	private Subject subject;

	public Clas getResult() {
		return clas;
	}

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public void startDocument() throws SAXException {
		clas = new Clas();
		teacher = new Teacher();
		student = new Student();
		subject = new Subject();
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch (qName) {
			case TEACHER_TAG:
				teacher = new Teacher();
			case STUDENTS_TAG:
				break;
			case CLAS_TAG:
				clas.setName(attributes.getValue("name"));
				break;
			case NAME_TAG:
			case AGE_TAG:
			case LICENSE_TAG:
				elementValue = new StringBuilder();
				break;
			case STUDENT_TAG:
				student = new Student();
				student.setName(attributes.getValue("name"));
				student.setAge(Integer.parseInt(attributes.getValue("age")));
				break;
			case SUBJECT_TAG:
				subject.setTitle(attributes.getValue("title"));
				subject.setMark(Integer.parseInt(attributes.getValue("mark")));
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
			case STUDENT_TAG:
				clas.addStudent(student);
				student = new Student();
				break;
			case NAME_TAG:
				teacher.setName(elementValue.toString());
				break;
			case AGE_TAG:
				teacher.setAge(Integer.parseInt(elementValue.toString()));
				break;
			case LICENSE_TAG:
				teacher.setLicense(Integer.parseInt(elementValue.toString()));
			case TEACHER_TAG:
				clas.setTeacher(teacher);
				//teacher = new Teacher();
				break;
			case SUBJECT_TAG:
				student.addSubject(subject);
				subject = new Subject();
				break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}
}