package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Clas;
import com.epam.rd.java.basic.task8.entities.Student;
import com.epam.rd.java.basic.task8.entities.Subject;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class STAXFileWriter {
    private static final String CLAS_TAG = "tn:class";
    private static final String TEACHER_TAG = "teacher";
    private static final String NAME_TAG = "name";
    private static final String AGE_TAG = "age";
    private static final String LICENSE_TAG = "license";
    private static final String STUDENTS_TAG = "students";
    private static final String STUDENT_TAG = "student";
    private static final String SUBJECT_TAG = "subject";
    private final String outputXmlFile;
    private static ByteArrayOutputStream byteArrayOutputStream;

    public STAXFileWriter(String outputXmlFile) {
        this.outputXmlFile = outputXmlFile;
        byteArrayOutputStream = new ByteArrayOutputStream();
    }

    public void writeClasToXml(Clas clas) {
        try {
            writeClasToBAOS(clas);

            String xml = byteArrayOutputStream.toString(StandardCharsets.UTF_8);
            String prettyXml = writePrettyXml(xml);

            Files.writeString(Path.of(outputXmlFile), prettyXml, StandardCharsets.UTF_8);
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeClasToBAOS(Clas clas) {
        try {
            XMLOutputFactory factory = XMLOutputFactory.newFactory();
            XMLStreamWriter writer =
                    factory.createXMLStreamWriter(byteArrayOutputStream);
            writer.writeStartDocument("UTF-8", "1.0");
            writer.writeStartElement(CLAS_TAG);
            writer.writeAttribute("name", clas.getName());
            //xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.writeAttribute("xsi:schemaLocation", "http://my.com input.xsd");
            writer.writeAttribute("xmlns:tn", "http://my.com");

            //Writing teacher Tag
            writer.writeStartElement(TEACHER_TAG);
            //Writing teacher name
            writer.writeStartElement(NAME_TAG);
            writer.writeCharacters(clas.getTeacher().getName());
            writer.writeEndElement();
            //Writing teacher age
            writer.writeStartElement(AGE_TAG);
            writer.writeCharacters(String.valueOf(clas.getTeacher().getAge()));
            writer.writeEndElement();
            //Writing teacher license
            writer.writeStartElement(LICENSE_TAG);
            writer.writeCharacters(String.valueOf(clas.getTeacher().getLicense()));
            writer.writeEndElement();
            //Close teacher tag
            writer.writeEndElement();

            //Writing students
            writer.writeStartElement(STUDENTS_TAG);

            for (int i = 0; i < clas.getStudents().size(); i++) {
                Student student = clas.getStudents().get(i);
                writer.writeStartElement(STUDENT_TAG);
                writer.writeAttribute("name", student.getName());
                writer.writeAttribute("age", String.valueOf(student.getAge()));

                for(int j = 0; j < student.getSubjects().size(); j++) {
                    Subject subject = student.getSubjects().get(j);
                    writer.writeEmptyElement(SUBJECT_TAG);
                    writer.writeAttribute("title", subject.getTitle());
                    writer.writeAttribute("mark", String.valueOf(subject.getMark()));
                }
                //Close student tag
                writer.writeEndElement();
            }
            //Close students tag
            writer.writeEndElement();
            //Close root clas tag
            writer.writeEndElement();
            writer.writeEndDocument();

            writer.flush();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private static String writePrettyXml(String xml) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));

        return output.toString();
    }
}
