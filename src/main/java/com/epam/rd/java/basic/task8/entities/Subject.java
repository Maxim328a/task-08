package com.epam.rd.java.basic.task8.entities;

public class Subject {
    private String title;
    private Integer mark;

    public Subject(String title, Integer mark) {
        this.title = title;
        this.mark = mark;
    }

    public Subject() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "title='" + title + '\'' +
                ", mark=" + mark +
                '}';
    }
}
