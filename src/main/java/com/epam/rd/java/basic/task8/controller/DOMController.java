package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entities.Clas;
import com.epam.rd.java.basic.task8.entities.Student;
import com.epam.rd.java.basic.task8.entities.Subject;
import com.epam.rd.java.basic.task8.entities.Teacher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private Clas clas;
	private Teacher teacher;
	private Student student;
	private Subject subject;

	private static final String TEACHER_TAG = "teacher";
	private static final String NAME_TAG = "name";
	private static final String AGE_TAG = "age";
	private static final String LICENSE_TAG = "license";
	private static final String STUDENTS_TAG = "students";
	private static final String STUDENT_TAG = "student";
	private static final String SUBJECT_TAG = "subject";

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Clas getResult() {
		doAction();
		return clas;
	}

	private void doAction() {
		clas = new Clas();
		teacher = new Teacher();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File(xmlFileName));
			doc.getDocumentElement().normalize();

//			System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
//			System.out.println("--------");
			clas.setName(doc.getDocumentElement().getAttribute("name"));

			NodeList teacherList = doc.getElementsByTagName(TEACHER_TAG);
			Element teacherElement = (Element) teacherList.item(0);
			teacher.setName(teacherElement.getElementsByTagName(NAME_TAG)
					.item(0).getTextContent());
			teacher.setAge(Integer.parseInt(teacherElement.getElementsByTagName(AGE_TAG)
					.item(0).getTextContent()));
			teacher.setLicense(Integer.parseInt(teacherElement
					.getElementsByTagName(LICENSE_TAG).item(0).getTextContent()));
			clas.setTeacher(teacher);

			NodeList studentsNL = doc.getElementsByTagName(STUDENTS_TAG);
			Element students = (Element) studentsNL.item(0);

			studentsNL = students.getElementsByTagName(STUDENT_TAG);
			for (int i = 0; i < studentsNL.getLength(); i++) {
				student = new Student();
				Element studentEl = (Element) studentsNL.item(i);
				student.setName(studentEl.getAttribute("name"));
				student.setAge(Integer.parseInt(studentEl.getAttribute("age")));

				NodeList subjectsNL = studentEl.getElementsByTagName(SUBJECT_TAG);
				for (int j = 0; j < subjectsNL.getLength(); j++) {
					subject = new Subject();
					Element subjectEl = (Element) subjectsNL.item(j);
					subject.setTitle(subjectEl.getAttribute("title"));
					subject.setMark(Integer.parseInt(subjectEl.getAttribute("mark")));
					student.addSubject(subject);
				}
				clas.addStudent(student);
			}
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
	}

}
