package com.epam.rd.java.basic.task8.entities;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person{
    private List<Subject> subjects;

    public Student(String name, int age, List<Subject> subjects) {
        super(name, age);
        this.subjects = subjects;
    }

    public Student() {
        subjects = new ArrayList<>();
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "\nStudent{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", subjects=" + subjects +
                '}';
    }
}
