package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entities.Clas;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			return;
		}

		String xsdFileName = args[1];
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		if (!validateXMLSchema(xmlFileName, xsdFileName)) return;
		else System.out.println("Input XML is valid due to XSD");
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Clas clas = domController.getResult();
		//System.out.println(clas);

		// sort (case 1)
		clas.sortStudentsByName();
		
		// save
		String outputXmlFile = "output.dom.xml";
		STAXFileWriter staxFileWriter = new STAXFileWriter(outputXmlFile);
		staxFileWriter.writeClasToXml(clas);

		if (validateXMLSchema(outputXmlFile, xsdFileName))
			System.out.println(outputXmlFile + " is valid due to XSD");
		else System.out.println(outputXmlFile + " is not valid to XSD");

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(xmlFileName, saxController);
		clas = saxController.getResult();
//		System.out.println(clas);
		
		clas.sortStudentsBySubjectNumber();
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		staxFileWriter = new STAXFileWriter(outputXmlFile);
		staxFileWriter.writeClasToXml(clas);

		if (validateXMLSchema(outputXmlFile, xsdFileName))
			System.out.println(outputXmlFile + " is valid due to XSD");
		else System.out.println(outputXmlFile + " is not valid to XSD");
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		clas = staxController.getResult();
		
		clas.sortStudentsByAverageMark();
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		staxFileWriter = new STAXFileWriter(outputXmlFile);
		staxFileWriter.writeClasToXml(clas);

		if (validateXMLSchema(outputXmlFile, xsdFileName))
			System.out.println(outputXmlFile + " is valid due to XSD");
		else System.out.println(outputXmlFile + " is not valid to XSD");
	}

	private static boolean validateXMLSchema(String xmlPath, String xsdPath) {
		try {
			SchemaFactory factory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(new File(xsdPath));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlPath)));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
