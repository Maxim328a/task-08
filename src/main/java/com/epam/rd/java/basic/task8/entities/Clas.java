package com.epam.rd.java.basic.task8.entities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Clas {
    private String name;
    private Teacher teacher;
    private List<Student> students;

    public Clas(Teacher teacher, List<Student> students, String name) {
        this.teacher = teacher;
        this.students = students;
        this.name = name;
    }

    public Clas() {
        teacher = new Teacher();
        students = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Clas{" +
                "\nteacher=" + teacher +
                ", \nstudents=" + students +
                '}';
    }

    //Natural order
    public void sortStudentsByName() {
        students = students.stream()
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());
    }

    //Sorts by average mark. Students with higher marks - first
    public void sortStudentsByAverageMark() {
        students = students.stream().sorted((o1, o2) -> {
            int i = 0;
            double average1 = 0;
            for (Subject s:o1.getSubjects()) {
                average1 += s.getMark();
                i++;
            }
            average1 /= i;
            i = 0;
            double average2 = 0;
            for (Subject s:o2.getSubjects()) {
                average2 += s.getMark();
                i++;
            }
            average2 /= i;
            return Double.compare(average2, average1);
        }).collect(Collectors.toList());
    }

    //Students with less quantity of subjects - first
    public void sortStudentsBySubjectNumber() {
        students = students.stream()
                .sorted((Comparator.comparingInt(o -> o.getSubjects().size())))
                .collect(Collectors.toList());
    }
}
